$(document).ready(function () {


	//Toggle Modes
	$('body').on('click', '.button', function(){
		var $selectedButton = $(this);
		$('.table-type').attr('data-mode', 'false');
		$selectedButton.attr('data-mode', 'true');
		//$(this).attr('data-mode', $(this).attr('data-mode') == 'true' ? 'false' : 'true'); //toggle not needed here
		if ($selectedButton.hasClass('pagingTable')){
			showPagingTable();
		}
		if ($selectedButton.hasClass('bigTable')){
			showBigTable();
		}
		if ($selectedButton.hasClass('fullTable')){
			showFullTable();
		}
		//clear column selector
		$('.floatingMenu a[data-mode="false"]').trigger('click');
		$('.floatingMenu').hide();
	});

	$('body').on('contextmenu', 'thead.contextMenu', function(e){
		var menu = $('.floatingMenu');
		e.preventDefault();
		var posX = e.pageX,
			posY = e.pageY;
		menu.css('top', posY);
		menu.css('left', posX);
		menu.toggle();
	});

	$('body').on('click', '.floatingMenu a', function(e){
		e.preventDefault();
		// Get the column API object
		var column = $('#superTable').DataTable().column( $(this).attr('data-column') );
		$(this).attr('data-mode', $(this).attr('data-mode') == 'true' ? 'false' : 'true');
		console.log(this);
		// Toggle the visibility
		column.visible( ! column.visible() );
		$('.floatingMenu').toggle();
	});

	function showPagingTable(){
		function clearDom(){
			var domToInject = '<table id="superTable" class="display" cellspacing="0" width="100%"><thead><tr><th>Name</th><th>Position</th><th>Office</th><th>Extn.</th><th>Start date</th><th>Salary</th></tr></thead></table>';
			$('#superTable').DataTable().destroy();
			$('#superTable').html(domToInject);
			buildTable();
		}
		function buildTable() {
			$('#superTable').DataTable({
				"destroy":true,
				"bDestroy": true,
				"colReorder": true,
				"paging": true, //disable paging
				"deferRender":    true, //enable virtual scrolling
				"fixedHeader": true,
				"bInfo": false, //hide paging info
				"ajax": "data/sampleData.json",
				"columns": [
					{ "data": "name" },
					{ "data": "hr.position" },
					{ "data": "contact.0" },
					{ "data": "contact.1" },
					{ "data": "hr.start_date" },
					{ "data": "hr.salary" }
				]
			});
		}
		clearDom();
	}
	function showBigTable(){
		function clearDom(){
			var domToInject ='<table id="superTable" class="display nowrap" style="display: none;" cellspacing="0" width="100%"><thead><tr><th>ID</th><th>First name</th><th>Last name</th><th>ZIP / Post code</th><th>Country</th></tr></thead></table>';
			$('#superTable').DataTable().destroy();
			$('#superTable').html(domToInject);
			buildTable();
		}
		function buildTable(){
			$('#superTable').DataTable({
				"destroy": true,
				"bDestroy": true,
				"serverSide":true,
				"colReorder": true,
				"bFilter":false, //with server on we can not search on the client
				"paging": true, //disable paging
				"deferRender":    true, //enable virtual scrolling
				"ajax": function ( data, callback, settings ) {
					var out = [];

					for ( var i=data.start, ien=data.start+data.length ; i<ien ; i++ ) {
						out.push( [ i+'-1', i+'-2', i+'-3', i+'-4', i+'-5' ] );
					}

					setTimeout( function () {
						callback( {
							draw: data.draw,
							data: out,
							recordsTotal: 5000000,
							recordsFiltered: 5000000
						} );
					}, 50 );
				},
				"scroller": {
					loadingIndicator: true
				},
				"scrollY":"60vh",
				"scrollX":true,
				"fixedHeader": false,
				"bInfo": true
			});
		}
		clearDom();
	}
	function showFullTable(){
		function clearDom(){
			var domToInject = '<table id="superTable" class="display" cellspacing="0" width="100%"><thead class="contextMenu"><tr><th>Name</th><th>Position</th><th>Office</th><th>Extn.</th><th>Start date</th><th>Salary</th></tr></thead></table>';
			$('#superTable').DataTable().destroy();
			$('#superTable').html(domToInject);
			buildTable();
		}
		function buildTable() {
			$('#superTable').DataTable({
				"destroy":true,
				"bDestroy": true,
				"paging": true,
				"colReorder": true,
				"deferRender":    true, //enable virtual scrolling
				"scrollY": "60vh",
				"scroller": {
					loadingIndicator: true
				},
				"searchHighlight": true,
				"scrollX": true,
				"bInfo": true, //hide paging info
				"ajax": "data/sampleDataLarge.json",
				"columns": [
					{ "data": "name", "width":"20px" },
					{ "data": "hr.position" },
					{ "data": "contact.0" },
					{ "data": "contact.1" },
					{ "data": "hr.start_date" },
					{ "data": "hr.salary" }
				],
				"dom": "Z<'row'<'small-6 columns'l><'small-6 columns'f>r>t<'row'<'small-6 columns'i><'small-6 columns'p>>",
				"colResize": {
					"rtl": false,
					"resizeCallback": function(column) {
						console.log("Column Resized");
					}
				}
			});
		}

		clearDom();
	}
	showFullTable();
});
$(document).foundation();
/*
*
Done - 1.	Columns should be sortable, multi-sortable and should be able to custom sort functions
Done - 2.	Columns should be rearrange able using DND
 3.	Allow grouping by fields – multi level (currently expected to provide 2 level grouping)
 4.	Custom row heights for groups vs rows
Done - 5.	Virtual rendering (critical feature) handle up to 5000 rows with ease
Done - 6.	Fixed headers
Done - 7.	Columns can be toggled (hidden/shown) (using a column picker) ** Right click on search input for selector **
Done - 8.	Possible options are to provide an alternate paging feature (with 508 compatibility
 9.	Internal state management – Highlighted rows, expanded, collapsed grouped rows
Done - 10.	Search within with highlighting of matched text + navigation via keyboard + 508 compatibility
 11.	Feature for summary and aggregate rows that can be positions both at the top or bottom of the grids.
 12.	Fixed columns

 */