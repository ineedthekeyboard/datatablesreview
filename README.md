# Datatables feature demonstration #
=======
## A sampling of many features available on a dynamic table when using datatables. ##
-----------
### Build the Sample ###
  * Run 'npm install' 
  * Run 'bower install'
  * Run 'npm serve' to host the page
  * **Note: Gulp default will just compile scss.**
*******************************************
### Featured in this javascript grid/table example are the following features ###
  1.	(Finished) Columns should be sortable, multi-sortable and should be able to custom sort functions
  2.	(Finished) Columns should be rearrange able using DND
  3.	Allow grouping by fields – multi level (currently expected to provide 2 level grouping)
  4.	Custom row heights for groups vs rows
  5.	(Finished) Virtual rendering (critical feature) handle up to 5000 rows with ease
  6.	(Finished) Fixed headers
  7.	(Finished) Columns can be toggled (hidden/shown) (using a column picker) ** Right click on search input for selector **
  8.	(Finished) Possible options are to provide an alternate paging feature (with 508 compatibility
  9.	Internal state management – Highlighted rows, expanded, collapsed grouped rows
  10.	(Finished) Search within with highlighting of matched text + navigation via keyboard + 508 compatibility
  11.	Feature for summary and aggregate rows that can be positions both at the top or bottom of the grids.
  12.	Fixed columns
*******************************************
Some Features don't work on the code pen due to needed external libraries
See the codepen here(note that search highlighting currently does not work on the codepen due to missing deps):
http://s.codepen.io/ineedthekeyboard/debug/JXzRbY